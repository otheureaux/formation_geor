---
title: "Analyse spatiale : présentation et cas d'étude"
format: 
  html:
    toc: true
    toc_float: true
    theme: cosmo
    df_print: paged
editor: visual
---

# Analyse spatiale

## Introduction à l'analyse spatiale avec R

Objectifs et importance de l'analyse spatiale : qu'est-ce l'analyse spatiale et pourquoi elle est importante ? Exemples concrets pour illustrer son utilité.

## Manipulation de données spatiales avec R

Introduction aux données spatiales : types de données spatiales (vecteur et raster) avec des exemples simples. Utilisation du package sf : des exercices pratiques pour charger, visualiser et manipuler des données spatiales vecteur.

Tout d'abord je charge les packages nécessaires.

```{r}
pacman::p_load(
  sf,      # manipulation d'objets vecteurs
  terra,   # manipulation d'objets rasters
  dplyr,   # manipulation et la trnsformation des données
  mapview,  # affichage de cartes interactives
  spdep
)
```

```{r, eval = F}
# Avec cette ligne je télécharge l'emprise des départements sur mon ordinateur
download.file('https://gitlab.huma-num.fr/otheureaux/database_sf/-/raw/main/data/DEPARTEMENT_SIMPLIFY.gpkg?ref_type=heads&inline=false',  
              destfile = "data/DEPARTEMENT_SIMPLIFY.gpkg") # remplacez le lien de destination du fichier
```

```{r, eval = F}
# Avec cette ligne je charge le fichier dans RStudio
emprise_2154 <- sf::st_read("data/DEPARTEMENT_SIMPLIFY.gpkg") # remplacez le chemin de localisation du fichier

# Avec cette ligne j'affiche le fichier emprise_2154
mapview(emprise_2154)
```

Je souhaite sélection les départements d'Ile-de-France :

```{r, eval = F}
names(emprise_2154)
dep_IDF <- emprise_2154 %>% 
  filter(INSEE_DEP %in% c(75,77,78,91,92,93,94,95))
```

Visualisation de base : Comment créer des cartes simples avec ggplot2 et mapview.

```{r, eval = F}
mapview(dep_IDF)
```

## Concepts fondamentaux d'analyse spatiale

Analyse exploratoire des données spatiales : comment explorer et comprendre les données spatiales à travers des statistiques de base et des visualisations ?

Systèmes de coordonnées et projections : l'importance des systèmes de projection et comment les manipuler dans R de manière simple.

## Applications pratiques de l'analyse spatiale

Cas d'étude : un projet simple mais complet : l'analyse de l'accessibilité des services publics dans une ville.

### Préparation des données pour l'analyse spatiale : carte de densité avec la fonction focal() du package terra.

```{r}
# Chargement des données GTFS 
# Ici je charge le fichier des arrêts
stops <- read.csv('data/idfm_gtfs/stops.txt')
# Utilisation de distinct() pour garder des lignes uniques basées sur stop_name
# tout en conservant les autres colonnes
stops_unique <- stops %>% distinct(stop_name, .keep_all = TRUE)
# Création d'un objet sf en utilisant les colonnes stop_lon et stop_lat
# Les coordonnées sont en WGS84
stops_unique_sf <- stops_unique %>% sf::st_as_sf(coords = c("stop_lon", "stop_lat"), crs = 4326)
# mapview(stops_unique_sf)
```

```{r}
# Reprojection de la couche
stops_unique_sf <- stops_unique_sf %>%
  st_transform(crs = 2154) # Lambert 93, EPSG:2154

# Définition du raster
res <- 500

# Rasterisation de la couche vecteur des stops
rast_stops <- rast(stops_unique_sf, resolution = res)

# Add a new field with a constant value (e.g., 1) to the point data
stops_unique_sf$const_value <- 1

rastStops <- terra::rasterize(vect(stops_unique_sf), 
                              rast_stops, field = "const_value", 
                              background = 0)
crs(rastStops) <- "epsg:2154"
plot(rastStops)
```

```{r}
d <- 1000 # Valeur du diamètre du noyau. Cela signifie que pour chaque pixel, nous allons calculer la densité de traces GPS dans un voisinage de 1000 m.

w <- focalMat(rastStops, d = d, type = "circle") # circle signifie que la forme du noyau est circulaire (il pourrait aussi être rectangulaire).

densStops <- focal(x = rastStops, w = w, fun = sum) # Calcul de la statistique focale, qui correspond à notre densité de noyau.Pour chaque cercle, cette fonction calcule la somme des pixels. 

mapview(densStops)
# raster::writeRaster(densStops, 'processed_data/raster_densStops.tif')
```

### Interprétation des résultats

# Autocorrelation

## Chargement des données

```{r}
maisons_2023_filter <- read.csv('data/full_csv/full_light.csv')
maisons_2023_sf <- maisons_2023_filter %>% 
  st_as_sf(coords = c("longitude","latitude"), 
           crs = 4326) %>% # WGS84
  st_transform(2154) %>%
  filter(grepl("^35", code_commune) &
           valeur_fonciere < 30000000)
maisons_2023_sf$log_valeur_fonciere <- log(maisons_2023_sf$valeur_fonciere)
coords <- st_coordinates(maisons_2023_sf)
```

## Définition du voisinage pour le calcul des indices d'autocorrélation

Pour définir le voisinage il y a trois possibilités :  
* 1-Contiguité (rook, queen, etc.) 
* 2-Distance (distance euclidienne)  
* 3-Plus proches voisins  (calcul du nombre de voisins le plus proches)  


### Définition du voisinage par les plus proches voisins (k-cluster) - Méthode 3
```{r}
# Calculer les k plus proches voisins pour chaque point
k <- 10
neighbors <- spdep::knn2nb(spdep::knearneigh(coords, 
                               k = k) # Le nombre de plus proches voisins à trouver pour chaque observation
                    )
## Matrice de poids spatiale 
lw <- spdep::nb2listw(neighbors, 
                      style = "W" # poids standardisés (somme = 1) par ligne (la plus utilisée), permet de réduire les effets de bords
                      )
# Afficher un aperçu de la matrice de poids spatiale
print(lw)
```

### Indice I de MORAN

A définir

```{r, eval = F}
 # I de Moran : indicateur robuste compris entre -1 et 1. 
spdep::moran.plot(maisons_2023_sf$valeur_fonciere, lw , labels =FALSE )

## Indice I de Moran pour les prix de vente des maisons ----
# l'indice est un coefficient de corrélation entre un point et son voisinage
moran_test <- moran.test(maisons_2023_sf$valeur_fonciere, lw)

# Afficher les résultats
print(moran_test)

## Moran avec plusieurs niveau de k (corrélogramme) ----

# Initialiser une liste pour stocker les résultats
results <- data.frame(k = integer(), moran_i = numeric())

# Définir les valeurs de k que nous voulons tester
k_values <- seq(1, 100, by = 2)

# Boucle pour calculer l'indice de Moran pour chaque k
for (k in k_values) {
  # Calculer les voisins
  neighbors <- knn2nb(knearneigh(coords, k = k))
  
  # Créer la matrice de pondération
  lw <- nb2listw(neighbors, 
                 style = "W")
  
  # Calculer l'indice de Moran
  moran_test <- moran.test(maisons_2023_sf$valeur_fonciere, lw) # Remplacez coords$variable par la variable d'intérêt
  
  # Extraire l'indice de Moran et l'ajouter aux résultats
  results <- rbind(results, data.frame(k = k, moran_i = moran_test$estimate[1]))
}

# Visualiser les résultats
ggplot(results, aes(x = k, y = moran_i)) +
  geom_line() +
  geom_point() +
  labs(title = "Variation de l'indice de Moran en fonction du nombre de voisins",
       x = "Nombre de voisins (k)",
       y = "Indice de Moran")
```


# GWR

## Packages 

```{r}
pacman::p_load(sf, tmap, mapview, dplyr, spdep, ggplot2, 
               spatialreg, # régression spatiale
               spgwr, GWmodel) # GWR
```

## Chargement des données


### Chargement des prix  
```{r, cache = T, out.width='100%'}
maisons_2023_filter <- read.csv('data/full_csv/full_light.csv')
maisons_2023_filter$log_valeur_fonciere <- log(maisons_2023_filter$valeur_fonciere)

maisons_2023_sf <- maisons_2023_filter %>% 
  st_as_sf(coords = c("longitude","latitude"), 
           crs = 4326) %>% # WGS84
  st_transform(2154) %>%
  filter(grepl("^35", code_commune) &
           valeur_fonciere < 30000000)

plotly::plot_ly(maisons_2023_sf, x = ~log_valeur_fonciere) %>% 
  plotly::add_histogram()
```

### Chargement des gares

```{r, out.width='100%'}
gares <- st_read("~/OT/RAILENIUM/BDD_TELLI/GEOFER/gares_2154.gpkg",
                 as_tibble = T, stringsAsFactors = F) %>% 
  select(commune, code_com, nom_gare, code_uic, voy_2020) %>%
  filter(grepl("^35", code_com))
glimpse(gares)

plot(st_geometry(gares))
```

## Cartographie des données

```{r, out.width='100%', eval = T}
# Calculer les quantiles pour la variable `valeur_fonciere`
maisons_2023_sf$valeur_fonciere_quantile <- cut(maisons_2023_sf$valeur_fonciere, 
                                                breaks = quantile(maisons_2023_sf$valeur_fonciere, probs = seq(0, 1, 0.2)), 
                                                include.lowest = TRUE,
                                                labels = c("Q1", "Q2", "Q3", "Q4", "Q5"))
```


```{r, out.width='100%', eval = T}
# Activer le mode d'affichage
tmap_mode("view")
# maisons_2023_sf_select <- maisons_2023_sf %>%
#   select(valeur_fonciere, valeur_fonciere_quantile) %>%
#   slice(1:8074)
# maisons_2023_sf <- sf::st_cast(maisons_2023_sf, "POINT")
pal_col <-  c("blue", "green", "yellow", "orange", "red")
# Visualiser les points avec les quantiles
tm_shape(maisons_2023_sf) + tm_dots(col = "valeur_fonciere_quantile", 
                                           palette = pal_col,
                                           title = "Quantiles du Prix de Vente")+
# tm_shape(maisons_2023_sf) +
#   tm_dots(col = "valeur_fonciere_quantile",
#           palette = 'viridis',
#           title = "Quantiles du Prix de Vente 2023") +
# tm_layout(title = "Prix de Vente des Maisons en 2023 (par Quantiles)") +
  tm_shape(gares) + tm_dots(size = 0.1)
```

## GWR avec GWmodel 

### Calculer les distances entre les maisons et les gares

```{r, eval = T}
distances <- st_distance(maisons_2023_sf, gares)
min_distances <- apply(distances, 1, min)

# Ajouter les distances minimales aux données des maisons
maisons_2023_sf$distance_gare_proche <- min_distances
maisons_2023_sf$log_distance_gare_proche <- maisons_2023_sf$distance_gare_proche

## Convertir les coordonnées des maisons en matrice 
coords <- st_coordinates(maisons_2023_sf)
```

### Construction de la matrice de distances entre toutes les observations
```{r, out.width='100%', eval = T}
# convertir l’objet sf en objet sp
maisons_2023_sp <- as_Spatial(maisons_2023_sf) # le package GWmodel n'est pas compatible avec 'sf'

# Construction de la matrice de distances entre toutes les observations
matDist <- gw.dist(dp.locat = coordinates(maisons_2023_sp))
```

### calcul de la bande passante, pondération spatiale

**Exponential**
```{r, out.width='100%', cache = T, eval = T}
nNeigh.exp <- bw.gwr(formula = log_valeur_fonciere ~ log_distance_gare_proche,
                     data = maisons_2023_sp, 
                     approach = "AICc", # CV ou AICc
                     kernel = "exponential", # forme de la fonction de pondération
                     adaptive = TRUE,
                     dMat = matDist)
```

**Gaussian**
```{r, out.width='100%', cache = T, eval = T}


nNeigh.gauss <- bw.gwr(formula = log_valeur_fonciere ~ log_distance_gare_proche,
                     data = maisons_2023_sp, 
                     approach = "AICc",
                     kernel = "gaussian",
                     adaptive = TRUE,
                     dMat = matDist)
```


### Estimation de la GWR 

**avec pondération exponential**
```{r, out.width='100%', cache = T, eval = T}
GWR_exp <- gwr.basic(data = maisons_2023_sp, 
                     bw = nNeigh.exp, 
                     kernel = "exponential", 
                     adaptive = TRUE,  dMat = matDist, 
                     formula = log_valeur_fonciere ~ log_distance_gare_proche)
```
**avec pondération exponential**
```{r, out.width='100%', cache = T, eval = T}
GWR_gauss <- gwr.basic(data = maisons_2023_sp, 
                     bw = nNeigh.exp, 
                     kernel = "gaussian", 
                     adaptive = TRUE,  dMat = matDist, 
                     formula = log_valeur_fonciere ~ log_distance_gare_proche)
```

**Comparaison des deux calibrations**
```{r}
diagGwr <- cbind(
  rbind(nNeigh.exp, nNeigh.gauss),
  rbind(GWR_exp$GW.diagnostic$gw.R2, GWR_gauss$GW.diagnostic$gw.R2),
  rbind(GWR_exp$GW.diagnostic$AIC, GWR_gauss$GW.diagnostic$AIC)) %>% 
  `colnames<-`(c("Nb Voisins","R2","AIC")) %>% 
  `rownames<-`(c("EXPONENTIAL","GAUSSIAN"))
diagGwr
```



```{r, out.width='100%', cache = T, eval = T}
GWR_exp
```

### Fonction de cartographie automatique des coefficients GWR
```{r, out.width='100%', cache = T, eval = T}

mapGWR <- function(spdf,var,var_TV,legend.title = "betas GWR",
                   main.title, dot.size = 0.1) {
  tv <- spdf[abs(var_TV)>1.96,]
  tm_shape(spdf) +
    tm_dots(var, title = legend.title, size = dot.size) +
    tm_shape(gares) + tm_dots(col="grey40") +
    tm_layout(title = main.title, legend.title.size =0.9, 
              inner.margins = .15) 
}

# Cartographie
tmap_mode("view")
a <- mapGWR(GWR_exp$SDF, 
            var = "log_distance_gare_proche",
            var_TV = GWR_exp$SDF$log_distance_gare_proche_TV,
            main.title = "Distance à la gare")

a
```

### Fonction de cartographie automatique des résidus
```{r, out.width='100%', cache = T, eval = T}

# Affichage de la carte des résidus
tm_shape(GWR_exp$SDF) +
  tm_dots(col = 'residual', size = 0.1) +
  tm_layout(title = "Résidus de la GWR", 
            legend.title.size = 0.9, 
            inner.margins = .15)
```
